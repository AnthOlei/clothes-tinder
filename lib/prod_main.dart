import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'main.dart';

void main(){
   WidgetsFlutterBinding.ensureInitialized();
  setupLocator(production: true);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}