import 'package:auto_route/auto_route.dart';
import 'package:clothes_tinder/features/app_boot_and_close/domain/lifecycle_event_handler.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/router/router.gr.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  WidgetsBinding.instance.addObserver(LifecycleEventHandler());
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: ExtendedNavigator<Router>(router: Router()),
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'AvenirNext',
        primaryColor: AppColors.primary,
        accentColor: AppColors.secondary,
        textSelectionColor: AppColors.secondary,
        cursorColor: AppColors.secondary,
      ),
    );
  }
}
