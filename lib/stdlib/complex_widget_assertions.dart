bool bothOrNone(Object first, Object second){
  if (first != null){
    return second != null;
  } else {
    return second == null;
  }
}