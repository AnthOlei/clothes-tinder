import 'package:auto_route/auto_route.dart';
import 'package:auto_route/auto_route_annotations.dart';
import 'package:clothes_tinder/features/app_boot_and_close/display/spash_screen.dart';
import 'package:clothes_tinder/features/list_view/presentation/list_page.dart';
import 'package:clothes_tinder/stdlib/ui/action_helpers/app_helper_wrapper.dart';

@CustomAutoRouter()
class $Router {
  @initial
  SplashScreen splashScreen;
  @CustomRoute(
      transitionsBuilder: TransitionsBuilders.fadeIn,
      durationInMilliseconds: 1000)
  AppHelperWrapper app;
  ListPage list;
}
