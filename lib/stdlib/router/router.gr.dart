// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:clothes_tinder/features/app_boot_and_close/display/spash_screen.dart';
import 'package:clothes_tinder/stdlib/ui/action_helpers/app_helper_wrapper.dart';
import 'package:clothes_tinder/features/list_view/presentation/list_page.dart';
import 'package:clothes_tinder/stdlib/models/product_list.dart';

abstract class Routes {
  static const splashScreen = '/';
  static const app = '/app';
  static const list = '/list';
  static const all = {
    splashScreen,
    app,
    list,
  };
}

class Router extends RouterBase {
  @override
  Set<String> get allRoutes => Routes.all;

  @Deprecated('call ExtendedNavigator.ofRouter<Router>() directly')
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.splashScreen:
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              SplashScreen(),
          settings: settings,
        );
      case Routes.app:
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              AppHelperWrapper(),
          settings: settings,
          transitionsBuilder: TransitionsBuilders.fadeIn,
          transitionDuration: const Duration(milliseconds: 1000),
        );
      case Routes.list:
        if (hasInvalidArgs<ListPageArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<ListPageArguments>(args);
        }
        final typedArgs = args as ListPageArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              ListPage(list: typedArgs.list),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

// *************************************************************************
// Arguments holder classes
// **************************************************************************

//ListPage arguments holder class
class ListPageArguments {
  final ProductList list;
  ListPageArguments({@required this.list});
}
