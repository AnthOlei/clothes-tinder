class Failure {
  String message;
  bool resolved = true;
  
  Failure(this.message, this.resolved);
}