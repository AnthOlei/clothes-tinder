abstract class Config{
  String get restURL;
  bool get playTaxingAnimations;
}

class DevConfig implements Config{
  @override
  String get restURL => "http://localhost:8080/";

  @override
  bool get playTaxingAnimations => false;
}

class ProdConfig implements Config{
  @override
  String get restURL => "URL";

  @override
  bool get playTaxingAnimations => true;
}