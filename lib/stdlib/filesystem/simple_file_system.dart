import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

class NoFileException implements Exception {
  String message;

  NoFileException(String filename){
    message = "couldn't open file with name $filename";
  }
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

//all files are saved in json format.
//@param fileName: name of file with NO extention, i.e. "user"
//TODO convert to enum
Future<File> _getFile(String name) async {
  final path = await _localPath;
  return File('$path/$name.json');
}

Future<void> writeToFileSystem(
    String fileName, Map<String, dynamic> data) async {
  final file = await _getFile(fileName);
  file.writeAsStringSync(json.encode(data));
}

Future<dynamic> readFromFileSystem(String fileName) async {
  try {
    final file = await _getFile(fileName);
    final contents = await file.readAsString();
    return json.decode(contents);
  } on FileSystemException catch (e) {
    if (e.osError.errorCode == 2){
      throw NoFileException(fileName);
    } else {
      rethrow;
    }
  }
}
