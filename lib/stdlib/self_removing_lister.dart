import 'package:flutter/material.dart';

//creates a listener that only triggers once, and then removes itself
void createSelfRemovingListener(void Function() functionToCall, ChangeNotifier notifier){
  void Function() f;
  f = (){
    functionToCall();
    notifier.removeListener(f);
  };
  notifier.addListener(f);
}