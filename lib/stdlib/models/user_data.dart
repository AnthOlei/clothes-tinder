import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_data.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class UserData{
  String uuid;
  List<int> seenProducts;
  List<int> likes;
  List<int> dislikes;
  Map<String, List<String>> lists;

  UserData({this.uuid, this.seenProducts, this.likes, this.dislikes, this.lists});

  factory UserData.fromJson(Map<String, dynamic> json) =>
      _$UserDataFromJson(json);
  Map<String, dynamic> toJson() => _$UserDataToJson(this);
}