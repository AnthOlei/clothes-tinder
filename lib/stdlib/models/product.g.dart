// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    title: json['title'] as String,
    source: json['source'] as String,
    images: (json['images'] as List)
        ?.map((e) =>
            e == null ? null : ImageData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    price: (json['price'] as num)?.toDouble(),
    sale: (json['sale'] as num)?.toDouble(),
    details: json['details'] as String,
    seller: json['seller'] as String,
  )
    ..id = json['id'] as int
    ..manufacturer = json['manufacturer'] as String
    ..kind = json['kind'] as String
    ..brand = json['brand'] == null
        ? null
        : Brand.fromJson(json['brand'] as Map<String, dynamic>)
    ..color = json['color'] as String;
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'source': instance.source,
      'images': instance.images,
      'price': instance.price,
      'sale': instance.sale,
      'details': instance.details,
      'manufacturer': instance.manufacturer,
      'kind': instance.kind,
      'brand': instance.brand,
      'color': instance.color,
      'seller': instance.seller,
    };
