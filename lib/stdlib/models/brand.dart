import 'package:json_annotation/json_annotation.dart';
part 'brand.g.dart';


@JsonSerializable(fieldRename: FieldRename.snake)
class Brand {
  String name;
  String logoLocation;

  Brand({this.name, this.logoLocation});

  factory Brand.fromJson(Map<String, dynamic> json) => _$BrandFromJson(json);
  Map<String, dynamic> toJson() => _$BrandToJson(this);
}