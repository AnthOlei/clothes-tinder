import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/models/product_list.dart';

class ProductLists {
  List<ProductList> productLists = [];

  ProductLists(){
        productLists.add(ProductList(
          name: "Your Likes",
          products: [],
        ));
  }

  ProductLists.debug() {
    productLists.add(ProductList(
          name: "Your Likes",
          products: [],
        ));
    productLists.add(ProductList(
          name: "Mom's birthday",
          products: [],
        ));
        
    productLists.add(ProductList(
          name: "Hawaii",
          products: [],
        ));
    productLists.add(ProductList(
          name: "Cute shoes",
          products: [],
        ));
    productLists.add(ProductList(
          name: "Bikini",
          products: [],
        ));
    productLists.add(ProductList(
          name: "Testing out the product lists",
          products: [],
        ));
  }

  void addToListWithName(Product product, String listName){
    for (final list in productLists){
      if (list.name == listName){
        list.products.add(product);
        return;
      }
    }
    throw Exception("List with name $listName not found. product could not be added");
  }


  List<String> getListNames(){
    List<String> retval = [];
    productLists.forEach((productList) => retval.add(productList.name));
    return retval;
  }
}
