import 'package:json_annotation/json_annotation.dart';

part 'image_data.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ImageData {
  String url;

  ImageData({this.url});

  factory ImageData.fromJson(Map<String, dynamic> json) => _$ImageDataFromJson(json);
  Map<String, dynamic> toJson() => _$ImageDataToJson(this);
}