import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:flutter/cupertino.dart';

class ProductList {
  String name;
  Color color;
  List<Product> products;

  ProductList({this.name, this.color, this.products});
}