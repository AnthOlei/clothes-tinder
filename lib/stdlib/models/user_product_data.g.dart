// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_product_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserProductData _$UserProductDataFromJson(Map<String, dynamic> json) {
  return UserProductData(
    likes: (json['likes'] as List)?.map((e) => e as int)?.toList(),
    dislikes: (json['dislikes'] as List)?.map((e) => e as int)?.toList(),
    wentToSite: (json['went_to_site'] as List)?.map((e) => e as int)?.toList(),
    uuid: json['uuid'] as String,
  );
}

Map<String, dynamic> _$UserProductDataToJson(UserProductData instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'likes': instance.likes,
      'dislikes': instance.dislikes,
      'went_to_site': instance.wentToSite,
    };
