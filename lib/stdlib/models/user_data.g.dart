// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserData _$UserDataFromJson(Map<String, dynamic> json) {
  return UserData(
    uuid: json['uuid'] as String,
    seenProducts:
        (json['seen_products'] as List)?.map((e) => e as int)?.toList(),
    likes: (json['likes'] as List)?.map((e) => e as int)?.toList(),
    dislikes: (json['dislikes'] as List)?.map((e) => e as int)?.toList(),
    lists: (json['lists'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, (e as List)?.map((e) => e as String)?.toList()),
    ),
  );
}

Map<String, dynamic> _$UserDataToJson(UserData instance) => <String, dynamic>{
      'uuid': instance.uuid,
      'seen_products': instance.seenProducts,
      'likes': instance.likes,
      'dislikes': instance.dislikes,
      'lists': instance.lists,
    };
