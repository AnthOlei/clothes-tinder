import 'package:clothes_tinder/stdlib/models/image_data.dart';
import 'package:json_annotation/json_annotation.dart';

import 'brand.dart';

part 'product.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Product {
  int id;
  String title;
  String source;
  List<ImageData> images = [];
  double price;
  double sale;
  String details;
  String manufacturer;
  String kind;
  Brand brand;
  String color;
  String seller;

  Product(
      {this.title,
      this.source,
      this.images,
      this.price,
      this.sale,
      this.details,
      this.seller}) {
    if (details != null) {
      details = _filterString(details);
      details = _filterSpecials(details);
    }
    if (title != null) {
      title = _filterString(title);
    }
  }

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);

  String _filterString(String string) {
    return string.replaceAll("&amp;", "&");
  }

  //filters not required for encoding. For example, many brands owned
  //by shops will say "Our Very Own" and because we don't want to say that,
  //we should cut it out.
  String _filterSpecials(String string) {
    string.replaceAll("our very own ", "");
    string.replaceAll("our very own ", "");
    return string;
  }

  //overrides the whole product with data from another product.
  void override(Product product) {
    title = product.title;
    source = product.source;
    images = product.images;
    price = product.price;
    sale = product.sale;
    details = product.details;
    seller = product.seller;
  }
}
