import 'package:json_annotation/json_annotation.dart';

part 'user_product_data.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class UserProductData{
  String uuid;
  List<int> likes;
  List<int> dislikes;
  List<int> wentToSite;

  UserProductData({this.likes, this.dislikes, this.wentToSite, this.uuid});

  factory UserProductData.fromJson(Map<String, dynamic> json) =>
      _$UserProductDataFromJson(json);
  Map<String, dynamic> toJson() => _$UserProductDataToJson(this);
}