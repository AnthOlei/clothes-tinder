
import 'package:clothes_tinder/features/home/data/product_data_access.dart';
import 'package:clothes_tinder/stdlib/http_client/simple_http_client.dart';

Future<Map<dynamic, dynamic>> queryProducts(ProductQuery query) async {
  final httpClient = SimpleHttpClient.useDefaults();
  return httpClient.post("/products/", query.toJson());
}