import 'package:clothes_tinder/stdlib/config/config.dart';
import 'package:dio/dio.dart';

import '../locator.dart';

class SimpleHttpClient {
  Dio _dio;

  SimpleHttpClient({String baseUrl}) {
    _dio = Dio(BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 5000,
      headers: {},
    ));
  }

  SimpleHttpClient.useDefaults() {
    final config = locator<Config>();
    _dio = Dio(BaseOptions(
      baseUrl: config.restURL,
      connectTimeout: 5000,
      headers: {"content-type": "application/json"},
    ));
  }

  Future<Map<dynamic, dynamic>> post(String path, Map<String, dynamic> body) async {
    final response = await _dio.post(path, data: body);
    return response.data as Map<dynamic, dynamic>;
  }
}
