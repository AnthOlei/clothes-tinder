import 'package:clothes_tinder/features/home/data/product_data_access.dart';
import 'package:clothes_tinder/features/product_query/domain/product_query_holder.dart';
import 'package:get_it/get_it.dart';

import 'config/config.dart';
import 'models/product.dart';
import 'models/product_lists.dart';

GetIt locator = GetIt.I;

void setupLocator({bool production = false}){
  if (production){
   locator.registerLazySingleton<Config>(()=> ProdConfig());
  } else {
    locator.registerLazySingleton<Config>(()=> DevConfig());
  }
    locator.registerLazySingleton<ProductLists>(() => ProductLists());

    //top product
    locator.registerSingleton(Product());

    locator.registerSingleton(ProductQueryHolder(query: ProductQuery.empty()));
}