import 'package:flutter/material.dart';

enum AnimatedButtonSize { extrasmall, small, standard }

class AnimatedButton extends StatefulWidget {
  final IconData icon;
  final Color baseIconColor;
  final Color iconToColor;
  final Color backgroundColor;
  final double iconSizePercent;
  final bool disabled;
  final AnimatedButtonSize size;
  final void Function() onClick;

  const AnimatedButton(
      {this.icon,
      @required this.baseIconColor,
      this.iconToColor,
      this.size,
      this.backgroundColor,
      this.disabled = false,
      @required this.onClick,
      @required this.iconSizePercent})
      : assert(iconSizePercent <= 2);

  @override
  _AnimatedButtonState createState() => _AnimatedButtonState();
}

class _AnimatedButtonState extends State<AnimatedButton>
    with SingleTickerProviderStateMixin {
  Animation<num> scale;
  AnimationController scaleController;

  static const POP_ANIMATION_DURATION = Duration(milliseconds: 1);
  static const EASE_ANIMATION_DURATION = Duration(milliseconds: 100);
  static const BOUNCE_ANIMATION_DURATION = Duration(milliseconds: 400);

  @override
  void initState() {
    scaleController =
        AnimationController(vsync: this, duration: POP_ANIMATION_DURATION)
          ..addListener(() => setState(() {}));
    final Animation<double> curve = CurvedAnimation(
        parent: scaleController,
        curve: Curves.ease,
        reverseCurve: Curves.bounceIn);
    scale = Tween(begin: 1.0, end: 0.8).animate(curve);
    super.initState();
  }

  @override
  void dispose() {
    scaleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: _pointerDown,
      onPointerUp: _pointerUp,
      child: Transform.scale(
        scale: scale.value as double,
        child: Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: widget.backgroundColor ?? Colors.white,
              boxShadow: [
                BoxShadow(blurRadius: 3, color: Colors.black12, spreadRadius: 1)
              ]),
          child: Padding(
            padding: EdgeInsets.all(10.0 + _calcPaddingSizeDifference()),
            child: ShaderMask(
                shaderCallback: (Rect bounds) {
                  return RadialGradient(
                    center: Alignment.topLeft,
                    radius: 1,
                    colors: <Color>[
                      widget.baseIconColor,
                      widget.iconToColor ?? widget.baseIconColor
                    ],
                    tileMode: TileMode.clamp,
                  ).createShader(bounds);
                },
                child: Icon(
                  widget.icon,
                  color: Colors.white,
                  size: _getTotalSize() * widget.iconSizePercent,
                )),
          ),
        ),
      ),
    );
  }

  void _pointerDown(PointerDownEvent event) {
    scaleController.forward();
  }

  void _pointerUp(PointerUpEvent event) {
    //checks to see if its in the radius.
    //if it is, we can trigger the action.
    //if it doesn't, we shouldn't trigger anything.
    //added some safe area because fingers slip a bit
    //made radius 45 because it seems to be responding better.
    //open to UX.
    const double stillDownLocation = 45 * 1.4;

    //if it's outside the boundries, or disabled, we dont trigger
    if (widget.disabled ||
        event.localPosition.dx > stillDownLocation ||
        event.localPosition.dy > stillDownLocation ||
        event.localPosition.dx < 0 ||
        event.localPosition.dy < 0) {
      scaleController.duration = EASE_ANIMATION_DURATION;
      final Animation<double> curve = CurvedAnimation(
          parent: scaleController,
          curve: Curves.ease,
          reverseCurve: Curves.ease);
      scale = Tween(begin: 1.0, end: 0.8).animate(curve);
    } else {
      //if it's inside the boundries (click happened)
      scaleController.duration = BOUNCE_ANIMATION_DURATION;
      widget.onClick();
      final Animation<double> curve = CurvedAnimation(
          parent: scaleController,
          curve: Curves.ease,
          reverseCurve: Curves.bounceIn);
      scale = Tween(begin: 1.0, end: 0.8).animate(curve);
    }
    scaleController.reverse().then((_) {
      scaleController.duration = POP_ANIMATION_DURATION;
    });
  }

  double _calcPaddingSizeDifference() {
    return _getTotalSize() * (1 - widget.iconSizePercent) / 2;
  }

  double _getTotalSize() {
    double size = 45;
    switch (widget.size){
      case AnimatedButtonSize.extrasmall:
      size = 20;
      break;
      case AnimatedButtonSize.small:
      size = 35;
      break;
      case AnimatedButtonSize.standard:
      size = 45;
      break;
    }
    return size;
  }
}
