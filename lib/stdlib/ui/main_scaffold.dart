import 'package:clothes_tinder/features/home/presentation/home_page.dart';
import 'package:clothes_tinder/features/lists/presentation/lists_page.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

import 'app_colors.dart';

class MainScaffold extends StatefulWidget {
  @override
  _MainScaffoldState createState() => _MainScaffoldState();
}

class _MainScaffoldState extends State<MainScaffold> {
  int _selectedIndex = 0;
  PageController pageController = PageController();

  //NOTE: NOT using material bottom sheet because I wanted custom control over animations ansd
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        extendBody: true,
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: SizedBox.expand(
            child: PageView(
              physics: const NeverScrollableScrollPhysics(),
              onPageChanged: _switchPage,
              controller: pageController,
              children: <Widget>[
                HomePage(screenSize: MediaQuery.of(context).size),
                ListsPage(),
              ],
            ),
          ),
        ),
        backgroundColor: AppColors.background,
        bottomNavigationBar: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 8),
            child: GNav(
                //TODO add a listenre that will allow swipes down here to switch pages
                gap: 8,
                activeColor: Colors.white,
                backgroundColor: Colors.transparent,
                iconSize: 24,
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: const Duration(milliseconds: 800),
                tabBackgroundColor: Colors.grey[800],
                tabs: [
                  GButton(
                    iconColor: AppColors.primary,
                    backgroundColor: AppColors.primary,
                    icon: Icons.favorite,
                    text: 'Browse',
                  ),
                  GButton(
                    iconColor: AppColors.primary,
                    backgroundColor: AppColors.primary,
                    icon: Icons.shopping_cart,
                    text: 'Your Lists',
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  _switchPage(index as int);
                }),
          ),
        ),
      ),
    );
  }

  void _switchPage(int index) {
    setState(() {
      _selectedIndex = index;
    });
    pageController.animateToPage(_selectedIndex,
        curve: Curves.easeOutExpo, duration: const Duration(milliseconds: 300));
  }
}
