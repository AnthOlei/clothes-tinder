import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomSheetActionProvider extends InheritedWidget {
  const BottomSheetActionProvider({
    Key key,
    @required this.dismiss,
    @required this.call,
    @required Widget child,
  }) : assert(child != null),
       super(key: key, child: child);

  final void Function() dismiss;
  final void Function(Widget) call;

  static BottomSheetActionProvider of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<BottomSheetActionProvider>();
  }

  @override
  bool updateShouldNotify(BottomSheetActionProvider old) => false;
}