import 'package:clothes_tinder/stdlib/ui/action_helpers/bottom_sheet/bottom_sheet_action_provider.dart';
import 'package:clothes_tinder/stdlib/ui/animated_flat_button.dart';
import 'package:clothes_tinder/stdlib/ui/main_scaffold.dart';
import 'package:flutter/material.dart';

import '../app_colors.dart';

//allows us to do many functions...
//but for now, we only have the ability to do:
// customBottomSheet using AppBottomSheetProvider.of(context).up(Widget)
class AppHelperWrapper extends StatefulWidget {
  @override
  _AppHelperWrapperState createState() => _AppHelperWrapperState();
}

class _AppHelperWrapperState extends State<AppHelperWrapper>
    with SingleTickerProviderStateMixin {
  Widget bottomSheetDisplayWidget = Container();
  bool bottomSheetIsUp = false;

  Animation<Offset> offset;
  AnimationController offsetController;

  @override
  void initState() {
    //offset controller
    offsetController = AnimationController(
      duration: const Duration(milliseconds: 700),
      vsync: this,
    )..addListener(() {
        setState(() {});
      });
    final Animation<double> topCardCurve = CurvedAnimation(
        parent: offsetController,
        curve: Curves.elasticOut,
        reverseCurve: Curves.easeInExpo);
    offset = Tween(begin: const Offset(0, 1.5), end: Offset.zero)
        .animate(topCardCurve);
    super.initState();
  }

  @override
  void dispose() {
    offsetController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        BottomSheetActionProvider(
          call: _callMe,
          dismiss: _dismiss,
          child: MainScaffold(),
        ),
        Positioned(
            // blackout curtain
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: AnimatedOpacity(
              duration: const Duration(milliseconds: 300),
              opacity: bottomSheetIsUp ? 1 : 0,
              child: bottomSheetIsUp
                  ? GestureDetector(
                      onTap: _dismiss,
                      child: Container(
                        color: Colors.black26,
                      ),
                    )
                  : Container(),
            )),
        Positioned(
          bottom: 20,
          left: 6,
          right: 6,
          child: SlideTransition(
            position: offset,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                boxShadow: [AppColors.getShadow()],
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    DefaultTextStyle(
                        style: TextStyle(
                            color: AppColors.text,
                            decoration: TextDecoration.none,
                            fontFamily: "AvenirNext",
                            fontSize: 18),
                        child: bottomSheetDisplayWidget),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: AnimatedFlatButton(
                        backgroundToColor: AppColors.secondary,
                        iconSizePercent: 1.0,
                        onClick: _dismiss,
                        baseBackgroundColor: AppColors.primary,
                        size: AnimatedButtonSize.small,
                        icon: Icons.arrow_downward,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _callMe(Widget widget) {
    if (bottomSheetIsUp) {
      debugPrint("attempted to call bottomSheetIsUpon an already bottomSheetIsUpcustom bottomSheet.");
      return;
    }
    setState(() {
      bottomSheetDisplayWidget = widget;
      offsetController.forward();
      bottomSheetIsUp = true;
    });
  }

  void _dismiss() {
    setState(() {
      offsetController.reverse();
      bottomSheetIsUp= false;
    });
  }
}

//  This file has a provider embedded in it...
// the blackout is beneath, and triggers the provder methods
