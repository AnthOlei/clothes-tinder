import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../app_colors.dart';



/**
 * if a launch url is provided, onpressed is ignored and 
 * the url is launched.
 */
void showAppDialog(
    {Widget body,
    String buttonText,
    void Function() onPressed,
    String title,
    BuildContext context, String launchUrl}) {
  showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          elevation: 20,
          title: Text(title),
          content: body,
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(context),
              splashColor: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text("Cancel".toUpperCase(),
                    style: TextStyle(
                      color: AppColors.primary,
                    )),
              ),
            ),
            FlatButton(
              color: AppColors.primary,
              onPressed: (){
                if (launchUrl == null){
                  onPressed();
                } else {
                  _launchURL(launchUrl);
                }
              },
              splashColor: AppColors.secondary,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  buttonText.toUpperCase(),
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        );
      });
}


_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
