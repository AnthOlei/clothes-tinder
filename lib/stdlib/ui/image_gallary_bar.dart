import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:flutter/material.dart';

class ImageGallaryBar extends StatelessWidget {
  final int totalPhotos; //exclusive upper bound
  final int currentImage; //from 0 to totalPhotos inclusive
  // ignore: constant_identifier_names
  static const BAR_HEIGHT = 5.0;

  const ImageGallaryBar({Key key, this.totalPhotos, this.currentImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildChildren(),
      ),
    );
  }

  //impure functions lol fuck the books
  List<Widget> _buildChildren() {
    if (totalPhotos == 1) {
      return [Container()];
    }

    final List<Widget> bars = [];
    for (int i = 0; i < totalPhotos; i++) {
      bars.add(Flexible(
        flex: totalPhotos,
        child: Container(
          height: BAR_HEIGHT,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              color: currentImage == i ? AppColors.secondary : Colors.black12),
        ),
      ));
      if (i != totalPhotos - 1){
        bars.add(Container(width: 5));
      }
    }
    return bars;
  }
}
