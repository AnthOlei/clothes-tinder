import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SecondaryPage extends StatelessWidget {
  final String title;
  final bool hasSearch;
  final String textfieldPlaceholder;
  final List<Widget> elements;
  final void Function(String) onSearch;

  const SecondaryPage(
      {this.title,
      this.hasSearch = false,
      this.textfieldPlaceholder = "",
      @required this.elements,
      this.onSearch});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                decoration:
                    BoxDecoration(color: AppColors.background, boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      spreadRadius: 1.5,
                      color: Colors.black12,
                      offset: const Offset(0, 1))
                ]),
                child: SafeArea(
                  bottom: false,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: GestureDetector(
                                onTap: () => Navigator.of(context).pop(),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Icon(
                                    Icons.navigate_before,
                                    color: AppColors.primary,
                                    size: 32,
                                  ),
                                )),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Text(
                            title,
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: AppColors.primary, fontSize: 24),
                          ),
                        ),
                        const Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
                child: ListView(
              padding: const EdgeInsets.all(2),
              children: _buildScrollElements(),
            )),
          ],
        ),
      ),
    );
  }

  //conver this to a builder!!
  //it'll get very, very long...
  List<Widget> _buildScrollElements() {
    final List<Widget> newElements = [];
    if (hasSearch) newElements.add(_buildTextField());
    newElements.addAll(elements);
    return newElements;
  }

  Widget _buildTextField() {
    return Container(
      decoration: const BoxDecoration(
          border:
              Border(bottom: BorderSide(color: Color(0xffe5e5e5), width: 1))),
      child: Padding(
          padding: const EdgeInsets.all(7.0),
          child: CupertinoTextField(
            placeholder: textfieldPlaceholder,
            onChanged: onSearch,
            prefix: Icon(Icons.search),
            decoration: const BoxDecoration(color: Color(0xfff2f2f2)),
            style: const TextStyle(fontFamily: "AvenirNext"),
          )),
    );
  }
}
