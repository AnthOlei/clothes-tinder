import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// I know this breaks the "no static only" rule, but come on. 
// it just works so well here
// ignore: avoid_classes_with_only_static_members
class AppColors {
  static Color primary = const Color(0xfff98b88);
  static Color secondary = const Color(0xffffc3a0);

  static Color background = const Color(0xffF7F8FB);
  static Color text = const Color(0xFF16161d);

  static Gradient gradient = LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      stops: const [0.1, 1.0],
      colors: const [Color(0xffFFAFBD), Color(0xffffc3a0)]);

  static List<Color> useColors = [
    Colors.red, 
    Colors.blueAccent,
    Colors.green,
    Colors.red,
    Colors.purpleAccent,
    Colors.deepPurple,
    Colors.deepOrange
  ];

  static Color getRandomColor(){
    return useColors[Random().nextInt(AppColors.useColors.length)];
  }

  static BoxShadow getShadow(){
    return BoxShadow(blurRadius: 3, color: Colors.black12, spreadRadius: 1);
  }
}
