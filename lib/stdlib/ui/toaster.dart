import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void errorToast(String message) {
  _toast(message, const Color(0xFFff6961), const Color(0xFF8b0000));
}

void successToast(String message) {
  _toast(message, const Color(0xFF90ee90), Colors.green);
}

void warningToast(String message) {
  _toast(message, const Color(0xFFffdb58), const Color(0xFFc49102));
}

void helpToast(String message) {
  _toast(message, const Color(0xFF80cee1), const Color(0xFF274e88));
}

void _toast(String message, Color bgColor, Color textColor) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIos: 4,
      backgroundColor: bgColor,
      textColor: textColor,
      fontSize: 16.0);
}
