import 'dart:ui';

import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product_list.dart';
import 'package:clothes_tinder/stdlib/models/product_lists.dart';
import 'package:clothes_tinder/stdlib/router/router.gr.dart';
import 'package:clothes_tinder/stdlib/ui/animated_button.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:flutter/material.dart';

class AppListTile extends StatelessWidget {
  final String name;
  final bool displayRemove;
  final Function(String) removeCallback;
  final bool special;

  const AppListTile(
      {this.name,
      this.removeCallback,
      this.displayRemove,
      this.special = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          _determineLeftWidget(),
          Expanded(
                      child: GestureDetector(
                        onTap: _navigateToList,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(Radius.circular(16)),
                              gradient: _determineColor(),
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 2,
                                    color: Colors.black12,
                                    offset: const Offset(0, 1),
                                    spreadRadius: 2)
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  name,
                                  style:
                                      const TextStyle(fontSize: 18, color: Colors.white),
                                ),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Colors.white,
                                      size: 24,
                                    ))
                              ],
                            ),
                          ),
                        ),
                      ),
          ),
        ],
      ),
    );
  }

  Widget _determineLeftWidget() {
    if (displayRemove) {
      return Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: AnimatedButton(
          baseIconColor: Colors.white,
          iconSizePercent: 1.4,
          icon: Icons.remove,
          onClick: () {
            removeCallback(name);
          },
          backgroundColor: const Color(0xffFF6961),
          size: AnimatedButtonSize.extrasmall,
        ),
      );
    } else {
      return Container();
    }
  }

  void _navigateToList() {
    final lists = locator<ProductLists>().productLists;
    ProductList productList;
    for (final list in lists) {
      if (list.name == name) {
        productList = list;
      }
    }
    Router.navigator.pushNamed("/list", arguments: productList);
  }

  LinearGradient _determineColor() {
    if (!special) {
      return LinearGradient(colors: [AppColors.primary, AppColors.secondary]);
    } else {
      return LinearGradient(colors: [Colors.green, Colors.green]);
    }
  }
}
