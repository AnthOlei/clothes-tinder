import 'package:auto_route/auto_route.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product_list.dart';
import 'package:clothes_tinder/stdlib/models/product_lists.dart';
import 'package:clothes_tinder/stdlib/router/router.gr.dart';
import 'package:clothes_tinder/stdlib/ui/action_helpers/dialog.dart';
import 'package:clothes_tinder/stdlib/ui/animated_button.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:clothes_tinder/stdlib/ui/toaster.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app_list_tile.dart';

class ListsPage extends StatefulWidget {
  @override
  _ListsPageState createState() => _ListsPageState();
}

class _ListsPageState extends State<ListsPage> {
  TextEditingController listTitleController = TextEditingController();
  bool toggleDelete = false;
  List<String> names;


  @override
  void initState() {
    names = locator<ProductLists>().getListNames();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Center(
          child: Text(
            "Your Lists",
            style: TextStyle(
                color: AppColors.primary,
                fontWeight: FontWeight.bold,
                fontSize: 34),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: AnimatedButton(
                onClick: () {
                  setState(() {
                    toggleDelete = !toggleDelete;
                  });
                },
                size: AnimatedButtonSize.small,
                icon: Icons.delete,
                baseIconColor: Colors.white,
                iconSizePercent: 1.1,
                backgroundColor: AppColors.primary,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: AnimatedButton(
                onClick: () {
                  _showAddDialog();
                },
                size: AnimatedButtonSize.small,
                icon: Icons.create,
                baseIconColor: Colors.white,
                iconSizePercent: 1.1,
                backgroundColor: AppColors.primary,
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
            itemBuilder: _buildTag,
            itemCount: names.length,
          ),
        ),
      ],
    );
  }

  Widget _buildTag(BuildContext context, int index){
    final String name = names[index];
    if (name == "Your Likes"){
      return AppListTile(name: name, removeCallback: (_){}, displayRemove: false, special: true);
    } else {
      return AppListTile(name: name, removeCallback: _showRemoveDialog, displayRemove: toggleDelete,);
    }
  }

  void _showAddDialog() {
    showAppDialog(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextField(
            decoration: const InputDecoration(hintText: "List name..."),
            controller: listTitleController,
          )
        ],
      ),
      buttonText: "Add list",
      context: context,
      title: "Add List",
      onPressed: _addList,
    );
  }

    void _showRemoveDialog(String name) {
    showAppDialog(
      body: Text("Remove list \"$name\"?"),
      buttonText: "Remove List",
      context: context,
      title: "Remove List",
      onPressed: (){_removeList(name);},
    );
  }

  void _addList() {
    final String name = listTitleController.value.text;
    if (name.length > 30){
      errorToast("Max length of list title is 30 chars.");
      return;
    }
    final List<ProductList> lists = locator<ProductLists>().productLists;
    for (final productList in lists){
      if (productList.name == name){
        errorToast("You already have a list with this name!");
        return;
      }
    }
    locator<ProductLists>()
        .productLists
        .add(ProductList(name: name, products: []));
    ExtendedNavigator.ofRouter<Router>().pop();
    listTitleController.clear();
    setState(() {
      names.add(name);
    });
  }

    void _removeList(String name) {
    final List<ProductList> lists = locator<ProductLists>().productLists;
    for (final productList in lists){
      if (productList.name == name){
        lists.remove(productList);
        break;
      }
    }
    ExtendedNavigator.ofRouter<Router>().pop();
        setState(() {
      names.remove(name);
    });
  }
}
