import 'package:after_layout/after_layout.dart';
import 'package:clothes_tinder/features/home/domain/triple_product_stack.dart';
import 'package:clothes_tinder/features/home/presentation/product_cards/card_action_bar/card_actions.dart';
import 'package:clothes_tinder/features/home/presentation/product_cards/dragged_product_cards/current_product_card.dart';

import 'package:clothes_tinder/features/home/presentation/product_cards/product_card.dart';
import 'package:clothes_tinder/features/home/presentation/product_cards/product_card_layout.dart';
import 'package:clothes_tinder/features/product_query/display/topbar.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/models/product_lists.dart';
import 'package:clothes_tinder/stdlib/self_removing_lister.dart';
import 'package:clothes_tinder/stdlib/ui/action_helpers/dialog.dart';
import 'package:clothes_tinder/stdlib/ui/toaster.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/product_bloc/product_bloc.dart';
import 'bloc/reset_bloc/reset_bloc.dart';
import 'bloc/throw_bloc/throw_bloc.dart';
import 'lists/list_list.dart';

class HomePage extends StatefulWidget {
  final Size screenSize;

  const HomePage({Key key, @required this.screenSize}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AfterLayoutMixin<HomePage>, TickerProviderStateMixin {
  //for laying out product card
  double productCardWidth = 0;
  double productCardHeight = 0;
  ProductCardLayout productCardLayout = ProductCardLayout();
  double lowerCardSize = 0.5;

  bool listsShown = false;
  AnimationController listsOffsetController;
  Animation<Offset> listsOffset;
  double listOpacity = 0;

  AnimationController prevCardAnimationController;
  Animation<Offset> prevCardOffset;

  Product previousProduct;

  final _productBloc = ProductBloc();
  final _resetBloc = ResetBloc();
  final _throwBloc = ThrowBloc();

  TripleProductStack _tripleProductStack;

  @override
  void initState() {
    // for lists
    _tripleProductStack = TripleProductStack(_productBloc);
    listsOffsetController = AnimationController(
      duration: const Duration(milliseconds: 400),
      vsync: this,
    )..addListener(() {
        setState(() {});
      });
    final Animation<double> curve = CurvedAnimation(
        parent: listsOffsetController,
        curve: Curves.elasticOut,
        reverseCurve: Curves.easeInExpo);
    listsOffset =
        Tween(begin: const Offset(1, 0), end: Offset.zero).animate(curve);

    //for top card for undo animation
    prevCardAnimationController = AnimationController(
      duration: const Duration(milliseconds: 700),
      vsync: this,
    )..addListener(() {
        setState(() {});
      });
    final Animation<double> topCardCurve = CurvedAnimation(
        parent: prevCardAnimationController,
        curve: Curves.elasticOut,
        reverseCurve: Curves.easeInExpo);
    prevCardOffset = Tween(begin: const Offset(-1.5, 0), end: Offset.zero)
        .animate(topCardCurve);
    super.initState();
  }

  @override
  void dispose() {
    listsOffsetController.dispose();
    prevCardAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            TopBar(),
            Expanded(
                child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: productCardLayout,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: BlocListener<ProductBloc, ProductState>(
                    bloc: _productBloc,
                    listener: (context, state) {
                      if (state is NoUndoState) {
                        warningToast("no previous product");
                      } else if (state is DisplayProductState) {
                        if (state.cardCameFrom == CardCameFrom.forward) {
                          final stateDefined = _tripleProductStack.curr;
                          setState(() {
                            if (stateDefined is DisplayProductState) {
                              previousProduct = stateDefined.product;
                            }
                            _productBloc.addSeen(
                                _tripleProductStack.bumpForward(state));
                            _resetBloc.add(ResetChildrenEvent());
                          });
                        } else if (state.cardCameFrom == CardCameFrom.undo) {
                          prevCardAnimationController.forward().then((_) {
                            _resetBloc.add(ResetChildrenEvent());
                            _productBloc.addNext(
                                _tripleProductStack.bumpBackward(state));
                            prevCardAnimationController.reset();
                            final peeked = _productBloc.peekPrevProduct();
                            if (peeked is DisplayProductState) {
                              previousProduct = peeked.product;
                            }
                          });
                        }
                      } else {
                        setState(() {
                          _productBloc
                              .addSeen(_tripleProductStack.bumpForward(state));
                        });
                      }
                    },
                    child: MultiBlocProvider(
                      providers: [
                        BlocProvider<ProductBloc>(
                          create: (BuildContext _) => _productBloc,
                        ),
                        BlocProvider<ResetBloc>(
                          create: (BuildContext _) => _resetBloc,
                        ),
                        BlocProvider<ThrowBloc>(
                          create: (BuildContext _) => _throwBloc,
                        ),
                      ],
                      child: Stack(
                        children: <Widget>[
                          _buildNextProduct(),
                          _buildCurrentProduct(),
                          _buildPrevProduct(),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )),
            CardActions(
              onDislike: _manualTriggerDislike,
              onLike: _manualTriggerLike,
              onBuyNow: () {
                _triggerBuyNowPopup(context);
              },
              onShowLists: _toggleList,
              onUndo: _onUndo,
            ),
          ],
        ),
        Align(
          alignment: Alignment.centerRight,
          child: SlideTransition(
            position: listsOffset,
            child: Opacity(
                opacity: listOpacity,
                child: ListList(onListClick: _addCurrentProductToList)),
          ),
        )
      ],
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      productCardWidth = productCardLayout.context.size.width;
      productCardHeight = productCardLayout.context.size.height;
    });
  }

  Widget _buildCurrentProduct() {
    final currState = _tripleProductStack.curr;
    if (currState is DisplayProductState) {
      return CurrentProductCard(
        screenSize: widget.screenSize,
        onRemoved: _onAnyAction,
        onAdded: _onAdded,
        percentAwayFromCenter: _scaleBottomCard,
        product: currState.product,
        height: productCardHeight,
        width: productCardWidth,
      );
    } else if (currState is NoProductState) {
      if (_productBloc.noMoreToFetch.value) {
        createSelfRemovingListener(_buildOnFetchCallback, _productBloc.hasNextProduct);
        return const Center(
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
                "No more products! Try loosening your search criteria, or try again later when we have fresh new products.",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 30)),
          ),
        );
      } else if (!_productBloc.noMoreToFetch.value &&
          _productBloc.isFetching.value) {
        createSelfRemovingListener(
            _buildOnFetchCallback, _productBloc.isFetching);
        return const Center(
          child: CupertinoActivityIndicator(
            animating: true,
          ),
        );
      } else {
        throw Exception(
            "there is more products to fetch but we are not fetching...");
      }
    }
    throw Exception("Invalid current state found in TripleProductState");
  }

  //crazy, crazy... callback that removes itself after the listener triggers
  //requird because if the product is fetching, we need to wait for the fetch
  //to complete, and then subsequent fetches should not trigger this callback
  //unless another loading animation gets through
  void _buildOnFetchCallback() {
    print("ran on fetch callback");
    _productBloc.add(NextProductEvent());
  }

  Widget _buildNextProduct() {
    final nextProductState = _tripleProductStack.next;
    if (nextProductState is DisplayProductState) {
      return Transform.scale(
        scale: lowerCardSize,
        alignment: Alignment.center,
        child: ProductCard(
          product: nextProductState.product,
          height: productCardHeight,
          width: productCardWidth,
          actionIcon: Icons.info,
          onActionTap: (){},
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _buildPrevProduct() {
    if (previousProduct != null) {
      return SlideTransition(
        position: prevCardOffset,
        child: ProductCard(
          product: previousProduct,
          height: productCardHeight,
          width: productCardWidth,
          actionIcon: Icons.info,
          onActionTap: () {
            debugPrint(
                "I am impressed that you hit the info button for this card...");
          },
        ),
      );
    } else {
      return Container();
    }
  }

  void _manualTriggerDislike() {
    _throwBloc.add(ThrowLeftEvent());
  }

  void _addCurrentProductToList(String listName) {
    final currState = _tripleProductStack.curr;
    if (currState is DisplayProductState) {
      locator<ProductLists>().addToListWithName(currState.product, listName);
      successToast("added ${currState.product.title} to list $listName!");
    }
    _manualTriggerLike();
  }

  void _manualTriggerLike() {
    _throwBloc.add(ThrowRightEvent());
  }

  void _triggerBuyNowPopup(BuildContext context) {
    final productCardState = _tripleProductStack.curr;
    if (productCardState is DisplayProductState) {
      showAppDialog(
          body: RichText(
            text: TextSpan(
                style:
                    DefaultTextStyle.of(context).style.copyWith(fontSize: 15),
                children: <TextSpan>[
                  TextSpan(
                      text: '"${productCardState.product.title}"',
                      style: const TextStyle(fontWeight: FontWeight.bold)),
                  const TextSpan(text: ' was found on '),
                  TextSpan(
                      text: '${productCardState.product.seller}.',
                      style: const TextStyle(fontWeight: FontWeight.bold)),
                  const TextSpan(
                      text: ' Would you like to continue to their website?'),
                ]),
          ),
          buttonText: "Continue",
          context: context,
          launchUrl: productCardState.product.source,
          title: "Continue With Purchase?");
    }
  }

  void _toggleList() {
    setState(() {
      listsShown = !listsShown;
      //these are processed with the NEW values,
      //so if (ListShown) means if we just tried to show the lists
      if (listsShown) {
        listOpacity = 1;
        listsOffsetController.forward();
      } else {
        listsOffsetController.reverse().then((_) {
          listOpacity = 0;
        });
      }
    });
  }

  void _onUndo() {
    _productBloc.add(UndoProductEvent());
  }

  void _onAdded() {
    final currState = _tripleProductStack.curr;
    if (currState is DisplayProductState) {
      locator<ProductLists>()
          .addToListWithName(currState.product, "Your Likes");
      _onAnyAction();
    } else {
      warningToast(
          "Oops! No product is displayed. Please try again when a product is displayed.");
    }
  }

  void _onAnyAction() {
    setState(() {
      listsShown = false;
      _productBloc.add(NextProductEvent());
      listsOffsetController.reverse().then((_) {
        listOpacity = 0;
      });
    });
  }

  void _scaleBottomCard(double scaleFactor) {
    setState(() {
      lowerCardSize = scaleFactor;
    });
  }
}
