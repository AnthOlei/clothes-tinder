import 'dart:math' as math;
import 'package:clothes_tinder/features/home/presentation/bloc/throw_bloc/throw_bloc.dart';
import 'package:clothes_tinder/features/home/presentation/product_cards/dragged_product_cards/product_card_stamp.dart';
import 'package:clothes_tinder/features/home/presentation/product_cards/product_card.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/ui/action_helpers/bottom_sheet/bottom_sheet_action_provider.dart';
import 'package:clothes_tinder/stdlib/ui/toaster.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

//For the top level product card. can be rotated and transformed, and thus
//should be a seperate widget so we aren't rebuilding the HomePage over
//and over

enum _DragPosition { stationary, left, middle, right }

class CurrentProductCard extends StatefulWidget {
  final Size screenSize;
  final void Function() onAdded;
  final void Function() onRemoved;
  final void Function(double) percentAwayFromCenter;
  final Product product;
  final double height; //TODO make these gotten from context?
  final double width;

  const CurrentProductCard(
      {@required this.product,
      @required this.screenSize,
      this.onAdded,
      this.onRemoved,
      this.percentAwayFromCenter,
      this.height,
      this.width});

  @override
  _CurrentProductCardState createState() => _CurrentProductCardState();
}

class _CurrentProductCardState extends State<CurrentProductCard>
    with TickerProviderStateMixin {
  // ignore_for_file: constant_identifier_names
  static const MAX_DEGREE_ROTATION = 15;
  //the % of how big the "middle" area is, where the swipes don't register as completed.
  static const UNITS_UNTIL_SWIPE_ACTION = 117;
  static const UNITS_UNTIL_SMALL_SWIPE = 5;
  static const SNAP_ANIMATION_DURATION = Duration(seconds: 1);
  static const THROW_ANIMATION_DURATION = Duration(milliseconds: 500);

  bool animating = false;

  AnimationController offsetController;
  Animation<Offset> offsetAnimated;

  AnimationController rotationController;
  Animation<num> rotationAnimated;

  Offset pointerDownLocation = Offset.zero;

  //for calcing swipes
  double rotationAngle = 0;
  Offset totalChangeInSwipe = const Offset(0, 0);

  double stampFactorLeft = 0;
  double stampFactorRight = 0;

  int imageIndex = 0;

  @override
  void initState() {
    super.initState();
    //only ONE controller needs to set state.
    offsetController = AnimationController(vsync: this)
      ..addListener(() => setState(() {
            _processBottomCardCallback(offsetAnimated.value.dx);
          }));
    rotationController = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    offsetController.dispose();
    rotationController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies(){
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ThrowBloc, ThrowState>(
      listener: (context, state) {
        if (state is ThrowLeftState){
          _generateThrowLeftEvent();
        } else if (state is ThrowRightState) {
          _generateThrowRightEvent();
        }
      },
          child: Column(
        children: <Widget>[
          Listener(
              behavior: HitTestBehavior.opaque,
              onPointerMove: _pointerDragged,
              onPointerUp: _pointerUp,
              onPointerDown: _pointerDown,
              //"after drag" translate
              child: Transform.translate(
                offset: animating ? offsetAnimated.value : totalChangeInSwipe,
                // "drag" rotate
                child: Transform.rotate(
                  angle: (animating ? rotationAnimated.value : rotationAngle) *
                      math.pi /
                      180,
                  child: Stack(
                    children: <Widget>[
                      ProductCard(
                        hasProductBloc: true,
                        preloadImages: true,
                        hasResetBloc: true,
                        height: widget.height,
                        width: widget.width,
                        product: widget.product,
                        actionIcon: Icons.info,
                        onActionTap: _displayInfo,
                      ),
                      ProductCardStamp(
                        alignment: Alignment.topRight,
                        icon: Icons.thumb_down,
                        color: Colors.red,
                        factor: stampFactorLeft,
                      ),
                      ProductCardStamp(
                        alignment: Alignment.topLeft,
                        icon: Icons.thumb_up,
                        color: Colors.green,
                        factor: stampFactorRight,
                      ),
                    ],
                  ),
                ),
              )),
        ],
      ),
    );
  }

  void _displayInfo() {
    if (widget.product.details != null && widget.product.details.isNotEmpty) {
      BottomSheetActionProvider.of(context).call(Padding(
        padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                widget.product.title,
                style: const TextStyle(fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
            ),
            Text(widget.product.details),
          ],
        ),
      ));
    } else {
      warningToast("There are no extra details for this product.");
    }
  }

  void _pointerDragged(PointerMoveEvent event) {
    _processBottomCardCallback(totalChangeInSwipe.dx);
    setState(() {
      totalChangeInSwipe = totalChangeInSwipe.translate(
          event.localDelta.dx, event.localDelta.dy);
      rotationAngle = totalChangeInSwipe.dx /
          (widget.screenSize.width / 2) *
          -(MAX_DEGREE_ROTATION / 2);
    });
  }

  void _pointerDown(PointerDownEvent event) {
    pointerDownLocation = event.position;
    if (animating) {
      //cancel animation for responsiveness
      setState(() {
        offsetController.reset();
        rotationController.reset();
        stampFactorLeft = 0;
        stampFactorRight = 0;
        animating = false;
      });
    }
  }

  void _pointerUp(PointerUpEvent event) {
    final _DragPosition dragPosition = _getDragPosition(event.position);
    switch (dragPosition) {
      case _DragPosition.middle:
        _animateSnapBack();
        break;
      case _DragPosition.left:
        _throwLeft(event);
        break;
      case _DragPosition.right:
        _throwRight(event);
        break;
      case _DragPosition.stationary:
        _animateSnapBack();
        break;
    }

    setState(() {
      rotationAngle = 0;
      pointerDownLocation = Offset.zero;
      totalChangeInSwipe = Offset.zero;
    });
  }

  _DragPosition _getDragPosition(Offset pointerUpLocation) {
    // calculate distance between two points.
    // if distance is < UNITS_UNTIL_SWIPE_ACTION,
    // it's not an action and should just trigger bounce back.
    // otherwise, we should see if we ended up left or right of the
    // inital point to see if we swiped left or right.

    //distance formula between points pointerDownLocation and pointerUpLocation
    final double distance = math.sqrt(
        math.pow(pointerDownLocation.dx - pointerUpLocation.dx, 2) +
            math.pow(pointerDownLocation.dy - pointerUpLocation.dy, 2));
    if (distance < UNITS_UNTIL_SMALL_SWIPE) {
      return _DragPosition.stationary;
    } else if (distance < UNITS_UNTIL_SWIPE_ACTION) {
      return _DragPosition.middle;
    } else if (pointerDownLocation.dx > pointerUpLocation.dx) {
      return _DragPosition.left;
    } else {
      return _DragPosition.right;
    }
  }

  void _generateThrowLeftEvent(){
      final double offsetY = widget.screenSize.height / 2 +
          ((math.Random().nextDouble() - 0.5) / 5) * widget.screenSize.height;
      final PointerUpEvent fakeEvent =
          PointerUpEvent(position: Offset(widget.screenSize.width, offsetY));
      _throwLeft(fakeEvent);
  }

  void _generateThrowRightEvent() {
      final double offsetY = widget.screenSize.height / 2 +
          ((math.Random().nextDouble() - 0.5) / 5) * widget.screenSize.height;
      final PointerUpEvent fakeEvent =
          PointerUpEvent(position: Offset(0, offsetY));
      _throwRight(fakeEvent);
  }

  void _resetCard() {
    setState(() {
      animating = false;
      offsetController.reset();
      rotationController.reset();
      stampFactorLeft = 0;
      stampFactorRight = 0;
      imageIndex = 0;
    });
  }

  //triggered when pointer is released from the left side. finishes animation,
  //and then pushes next in stack
  void _throwLeft(PointerUpEvent event) {
    final offsetY = event.position.dy - (widget.screenSize.height / 2);
    final targetY = offsetY * 3;
    final targetX = 0 - widget.width * 1.5;
    setState(() {
      offsetController.duration = THROW_ANIMATION_DURATION;
      rotationController.duration = THROW_ANIMATION_DURATION;
      animating = true;
      final Animation<double> curve =
          CurvedAnimation(parent: offsetController, curve: Curves.decelerate);
      offsetAnimated =
          Tween(begin: totalChangeInSwipe, end: Offset(targetX, targetY))
              .animate(curve);
      rotationAnimated =
          Tween(begin: rotationAngle, end: MAX_DEGREE_ROTATION * 2)
              .animate(curve);
      offsetController.forward().then((_) {
        widget.onRemoved();
        _resetCard();
      });
    });
  }

  void _throwRight(PointerUpEvent event) {
    final offsetY = event.position.dy - (widget.screenSize.height / 2);
    final targetY = offsetY * 3;
    //targetX is some fraction of the card width less than the border of the screen.
    final targetX = widget.screenSize.width + widget.width * 1.5;
    setState(() {
      offsetController.duration = THROW_ANIMATION_DURATION;
      rotationController.duration = THROW_ANIMATION_DURATION;
      animating = true;
      final Animation<double> curve =
          CurvedAnimation(parent: offsetController, curve: Curves.decelerate);
      offsetAnimated =
          Tween(begin: totalChangeInSwipe, end: Offset(targetX, targetY))
              .animate(curve);
      rotationAnimated =
          Tween(begin: rotationAngle, end: MAX_DEGREE_ROTATION * -2)
              .animate(curve);
      offsetController.forward().then((_) {
        widget.onAdded();
        _resetCard();
      });
    });
  }

  void _animateSnapBack() {
    setState(() {
      animating = true;
      offsetController.duration = SNAP_ANIMATION_DURATION;
      rotationController.duration = SNAP_ANIMATION_DURATION;
      final Animation<double> curve =
          CurvedAnimation(parent: offsetController, curve: Curves.elasticOut);
      offsetAnimated =
          Tween(begin: totalChangeInSwipe, end: Offset.zero).animate(curve);
      rotationAnimated = Tween(begin: rotationAngle, end: 0).animate(curve);

      //begin animating and reset
      offsetController.forward().then((_) => setState(() {
            animating = false;
            offsetController.reset();
            rotationController.reset();
          }));
    });
  }

  void _processBottomCardCallback(double distanceDrag) {
    //TODO fine tune these numbers.
    //perhaps make it some mathamatical formula?
    double calcedStampFactor = distanceDrag.abs() * (1 / 400);
    if (calcedStampFactor > 1) calcedStampFactor = 1.0;
    if (distanceDrag < 0) {
      setState(() {
        stampFactorLeft = calcedStampFactor;
      });
    } else if (distanceDrag > 0) {
      setState(() {
        stampFactorRight = calcedStampFactor;
      });
    }

    //for bottom card
    double sizeMult = (distanceDrag.abs() + 300) / 500;
    if (sizeMult > 1) sizeMult = 1.0;
    widget.percentAwayFromCenter(sizeMult);
  }
}
