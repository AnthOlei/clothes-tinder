import 'dart:math' as math;
import 'package:flutter/material.dart';

class ProductCardStamp extends StatelessWidget {
  final MaterialColor color;
  final IconData icon;
  final Alignment alignment;
  final double factor; //1 being all the way swiped, 0 being center

  const ProductCardStamp({this.color, this.icon, this.factor, this.alignment});

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: alignment,
        child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Transform.rotate(
              angle: 15 * math.pi / 180,
              child: Opacity(
                opacity: factor,
                              child: Transform.scale(
                  scale: 1.5 * factor, //scale max: 1.5
                                child: ShaderMask(
                    shaderCallback: (Rect bounds) {
                      return RadialGradient(
                        center: Alignment.center,
                        radius: 1,
                        stops: const [0.0, 1.0],
                        colors: <Color>[color[100], color[900]],
                        tileMode: TileMode.clamp,
                      ).createShader(bounds);
                    },
                    child: Icon(
                      icon,
                      size: 200,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            )));
  }
}
