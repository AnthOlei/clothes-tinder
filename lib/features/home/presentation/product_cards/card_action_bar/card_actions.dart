import 'package:clothes_tinder/stdlib/ui/animated_button.dart';
import 'package:flutter/material.dart';

class CardActions extends StatelessWidget {
  final void Function() onDislike;
  final void Function() onLike;
  final void Function() onShowLists;
  final void Function() onUndo;
  final void Function() onBuyNow;

  const CardActions({Key key, this.onDislike, this.onBuyNow, this.onLike, this.onShowLists, this.onUndo}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          AnimatedButton(
            icon: Icons.undo,
            baseIconColor: Colors.orange[200],
            iconToColor: Colors.orange[800],
            iconSizePercent: 1.0,
            size: AnimatedButtonSize.small,
            onClick: onUndo,
          ),
          AnimatedButton(
            onClick: onLike,
            icon: Icons.thumb_up,
            baseIconColor: Colors.green[200],
            iconToColor: Colors.green[800],
            iconSizePercent: 0.75,
          ),
          AnimatedButton(
            icon: Icons.shopping_cart,
            baseIconColor: Colors.blue[200],
            iconToColor: Colors.blue[800],
            iconSizePercent: 0.8,
            onClick: onBuyNow,
            size: AnimatedButtonSize.small,
          ),
          AnimatedButton(
            onClick: onDislike,
            icon: Icons.thumb_down,
            baseIconColor: Colors.red[200],
            iconToColor: Colors.red[500],
            iconSizePercent: 0.75,
          ),
          AnimatedButton(
            onClick: onShowLists,
            icon: Icons.add_to_photos,
            baseIconColor: Colors.purple[400],
            iconToColor: Colors.pink[800],
            iconSizePercent: 0.70,
            size: AnimatedButtonSize.small,
          ),
        ],
      ),
    );
  }
}