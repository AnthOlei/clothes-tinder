import 'package:clothes_tinder/features/home/presentation/bloc/product_bloc/product_bloc.dart';
import 'package:clothes_tinder/features/home/presentation/bloc/reset_bloc/reset_bloc.dart';
import 'package:clothes_tinder/stdlib/complex_widget_assertions.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:clothes_tinder/stdlib/ui/image_gallary_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ProductCard extends StatefulWidget {
  static const double _borderRadius = 16;
  final double height;
  final double width;
  final Product product;
  final IconData actionIcon;
  final void Function() onActionTap;
  final bool hasProductBloc;
  final bool preloadImages;
  final bool hasResetBloc;

  ProductCard({
    @required this.height,
    @required this.width,
    @required this.product,
    this.actionIcon,
    this.onActionTap,
    this.preloadImages = false,
    Key key,
    this.hasResetBloc = false,
    this.hasProductBloc = false,
  })  : assert(bothOrNone(actionIcon, onActionTap),
            "actionIcon and onAction must both be filled, or neither."),
        super(key: key);

  @override
  ProductCardState createState() => ProductCardState();
}

class ProductCardState extends State<ProductCard> {
  final NumberFormat _formatCurrency = NumberFormat.simpleCurrency();
  int currentImageIndex;
  List<Image> preloadedImages = [];
  bool haveBeenPreloaded = false;

  final List<Shadow> _textShadow = const [
    Shadow(blurRadius: 6, color: Colors.black38),
  ];

  @override
  void initState() {
    currentImageIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _wrapWithNecessaryWidgets(Container(
      height: widget.height,
      width: widget.width,
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 4, spreadRadius: 1, color: Colors.black12)
        ],
        borderRadius:
            BorderRadius.all(Radius.circular(ProductCard._borderRadius)),
        color: Colors.white,
      ),
      child: ClipRRect(
        borderRadius:
            const BorderRadius.all(Radius.circular(ProductCard._borderRadius)),
        child: Stack(
          children: <Widget>[
            SizedBox.expand(
              child: FittedBox(fit: BoxFit.fitHeight, child: _determineImage()),
            ),
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    Colors.transparent,
                    AppColors.primary.withOpacity(0.9)
                  ],
                      stops: const [
                    0.7,
                    1
                  ])),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      flex: 7,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            widget.product.title,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 36,
                                shadows: _textShadow),
                          ),
                          _buildPrice(),
                        ],
                      ),
                    ),
                    _reserveSpaceForActionButtonIfNecessary(),
                  ],
                ),
              ),
            ),
            Column(
              children: <Widget>[
                ImageGallaryBar(
                    totalPhotos: widget.product.images.length,
                    currentImage: currentImageIndex),
              ],
            ),
            Stack(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: GestureDetector(
                          onTap: _tapLeft,
                        )),
                    Flexible(
                        flex: 1,
                        child: GestureDetector(
                          onTap: _tapRight,
                        )),
                  ],
                ),
                _buildIconIfNecessary(),
              ],
            )
          ],
        ),
      ),
    ));
  }

  Widget _buildPrice() {
    if (widget.product.sale != null){
    return Row(
      children: [
        Text(
          "${_formatCurrency.format(widget.product.sale)}",
          style: TextStyle(color: Colors.red[800], fontSize: 32, shadows: _textShadow),
        ),
        Container(width: 10,),
                Text(
          "${_formatCurrency.format(widget.product.price)}",
          style: TextStyle(color: Colors.white, fontSize: 22, shadows: _textShadow, decoration: TextDecoration.lineThrough),
        ),
      ],
    );
    } else {
      return Text(
          "${_formatCurrency.format(widget.product.price)}",
          style: TextStyle(color: Colors.white, fontSize: 32, shadows: _textShadow),
        );
    }
  }

  Widget _determineImage() {
    if (widget.preloadImages && !haveBeenPreloaded) {
      //preload images, return one
      for (final imageData in widget.product.images) {
        final Image image = Image.network(imageData.url);
        precacheImage(image.image, context);
        preloadedImages.add(image);
      }
      haveBeenPreloaded = true;
      return preloadedImages[currentImageIndex];
    } else if (widget.preloadImages && haveBeenPreloaded) {
      return preloadedImages[currentImageIndex];
    } else {
      return Image.network(
          widget.product.images[currentImageIndex].url); //no preload
    }
  }

  Widget _wrapWithNecessaryWidgets(Widget child) {
    Widget returnMe;
    if (widget.hasProductBloc) {
      returnMe = BlocListener<ProductBloc, ProductState>(
        listener: (context, state) {
          if (state is DisplayProductState) {
            _resetCard();
          }
        },
        child: child,
      );
    } else {
      returnMe = child;
    }
    return _addListenerIfNecessary(child);
  }

  Widget _addListenerIfNecessary(Widget child){
    if (widget.hasResetBloc){
      return BlocListener<ResetBloc, ResetState>(
        listener: (context, state) {
          _resetCard();
        },
        child: child,
      );
    }  else {
      return child;
    }
  }

  void _resetCard() {
    setState(() {
      currentImageIndex = 0;
      haveBeenPreloaded = false;
      preloadedImages = [];
    });
  }

  Widget _buildIconIfNecessary() {
    if (widget.onActionTap != null) {
      return Positioned(
          bottom: 20,
          right: 20,
          child: GestureDetector(
              onTap: widget.onActionTap,
              child: Icon(
                widget.actionIcon,
                color: Colors.white.withOpacity(.95),
                size: 36,
              )));
    } else {
      return Container();
    }
  }

  Widget _reserveSpaceForActionButtonIfNecessary() {
    if (widget.actionIcon != null) {
      return const Spacer(
        flex: 3,
      );
    } else {
      return const Spacer();
    }
  }

//setstate not needed here because state is set
//in animations.
  void _tapLeft() {
    if (currentImageIndex > 0) {
      currentImageIndex--;
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.lightImpact',
      );
    } else {
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.mediumImpact',
      );
    }
  }

  void _tapRight() {
    if (currentImageIndex < widget.product.images.length - 1) {
      currentImageIndex++;
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.lightImpact',
      );
    } else {
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.mediumImpact',
      );
    }
  }
}
