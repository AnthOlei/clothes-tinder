import 'package:after_layout/after_layout.dart';
import 'package:clothes_tinder/stdlib/config/config.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:clothes_tinder/stdlib/ui/toaster.dart';
import 'package:flutter/material.dart';

enum ListTagSpeed{slow, medium, fast}

class ListTag extends StatefulWidget {
  final String name;
  final int delay;
  final ListTagSpeed listTagSpeed;
  final void Function(String) onClick;
  final bool disabled;

  const ListTag({this.name, @required this.delay, this.listTagSpeed, this.onClick, this.disabled});

  @override
  _ListTagState createState() => _ListTagState();
}

class _ListTagState extends State<ListTag>
    with TickerProviderStateMixin, AfterLayoutMixin<ListTag> {
  final double radius = 20;
  Animation<num> offsetY;
  Animation<num> scale;

  AnimationController offsetYController;
  AnimationController scaleController;

  @override
  void initState() {
    offsetYController =
        AnimationController(vsync: this, duration: const Duration(seconds: 1))
          ..addListener(() => setState(() {}));
    final Animation<double> offsetCurve = CurvedAnimation(
        parent: offsetYController,
        curve: Curves.easeInOut,
        reverseCurve: Curves.easeInOut);
    offsetY = Tween(begin: 2.0, end: -2.0).animate(offsetCurve);

      scaleController =
        AnimationController(vsync: this, duration: const Duration(milliseconds: 250))
          ..addListener(() => setState(() {}));
    final Animation<double> scaleCurve = CurvedAnimation(
        parent: scaleController,
        curve: Curves.decelerate);
    scale = Tween(begin: 1.0, end: 1.5).animate(scaleCurve);
    super.initState();
  }

  @override
  void dispose() {
    offsetYController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Transform.scale(
        scale: scale.value as double,
              child: Transform.translate(
          offset: Offset(0, offsetY.value as double),
          child: GestureDetector(
            onTap: onClick,
                    child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        spreadRadius: 2,
                        blurRadius: 3,
                        offset: const Offset(0, 4))
                  ],
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  gradient: LinearGradient(
                      colors: [AppColors.primary, AppColors.secondary])),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20),
                child: Text(
                  widget.name,
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.right,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if(locator<Config>().playTaxingAnimations){
    Future.delayed(Duration(milliseconds: 100 * widget.delay)).then((_){
      offsetYController.repeat(reverse: true);
    });
    }
  }

  void onClick(){
    //TODO is there something that I need to do with disabled here?
    setState(() {
      scaleController.forward().then((_){
        scaleController.reverse().then((_){
          widget.onClick(widget.name);
        });
      });
    });
  }
}
