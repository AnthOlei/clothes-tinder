import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product_lists.dart';
import 'package:clothes_tinder/stdlib/ui/animated_button.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:flutter/material.dart';

import 'list_tag.dart';

class ListList extends StatefulWidget {
  static const int startIndex = 0; //inclusive
  static const int displayTotal = 3; //inclusive
  final void Function(String) onListClick;

  const ListList({Key key, this.onListClick}) : super(key: key);

  @override
  _ListListState createState() => _ListListState();
}

class _ListListState extends State<ListList> {
  int currStart;

  bool topDisabled = false;
  bool bottomDisabled = false;
  List<String> names = locator<ProductLists>().getListNames();

  @override
  void initState() {
    currStart = ListList.startIndex;
    names.remove("Your Likes");
    _disableButtonsIfNecessary();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        _buildButton(Icons.arrow_drop_up, _scrollUp, disabled: topDisabled),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buildList(),
        ),
        _buildButton(Icons.arrow_drop_down, _scrollDown,
            disabled: bottomDisabled),
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<Widget> _buildList() {
    final List<Widget> builtTags = [];
    int delay = 0;
    for (int i = currStart; i < currStart + ListList.displayTotal && i < names.length; i++) {
        builtTags.add(_buildTag(names[i], delay));
      delay++;
    }
    if (builtTags.isEmpty) {
      builtTags.add(_buildTag(
          "You don't have any lists! click the cart on the bottom of your screen to add some.",
          0, disabled: true));
      setState(() {
        currStart = 0;
      });
      _disableButtonsIfNecessary();
    }
    return builtTags;
  }

  Widget _buildButton(IconData icon, void Function() onclick, {bool disabled}) {
    return Padding(
        padding: const EdgeInsets.only(right: 16.0, top: 8.0),
        child: AnimatedButton(
          iconSizePercent: 2,
          size: AnimatedButtonSize.extrasmall,
          onClick: onclick,
          baseIconColor: Colors.white,
          disabled: disabled,
          backgroundColor:
              disabled ? const Color(0xffECECEC) : AppColors.primary,
          icon: icon,
        ));
  }

  void _disableButtonsIfNecessary() {
    int totalTags = names.length;
    setState(() {
      topDisabled = currStart + ListList.displayTotal >= totalTags;
      bottomDisabled = 0 == currStart;
    });
  }

  void _scrollUp() {
    setState(() {
      currStart++;
    });
    _disableButtonsIfNecessary();
  }

  void _scrollDown() {
    setState(() {
      currStart--;
    });
    _disableButtonsIfNecessary();
  }

  //NEED INDEX PARAM
  Widget _buildTag(String name, int delay, {bool disabled = false}) {
    return ListTag(
      name: name,
      delay: delay,
      disabled: disabled,
      onClick: disabled ? (_){} : widget.onListClick,
    );
  }
}
