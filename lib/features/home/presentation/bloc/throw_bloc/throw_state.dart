part of 'throw_bloc.dart';

@immutable
abstract class ThrowState {}

class ThrowInitial extends ThrowState {}

class ThrowLeftState extends ThrowState {}

class ThrowRightState extends ThrowState {}