import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'throw_event.dart';
part 'throw_state.dart';

class ThrowBloc extends Bloc<ThrowEvent, ThrowState> {
  @override
  ThrowState get initialState => ThrowInitial();

  @override
  Stream<ThrowState> mapEventToState(
    ThrowEvent event,
  ) async* {
    if (event is ThrowLeftEvent){
      yield ThrowLeftState();
    } else if (event is ThrowRightEvent){
      yield ThrowRightState();
    }
  }
}
