part of 'throw_bloc.dart';

@immutable
abstract class ThrowEvent {}

class ThrowLeftEvent extends ThrowEvent {}

class ThrowRightEvent extends ThrowEvent {}