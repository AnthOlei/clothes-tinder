part of 'product_bloc.dart';

@immutable
abstract class ProductEvent {}

class UndoProductEvent extends ProductEvent {}

class NextProductEvent extends ProductEvent {}