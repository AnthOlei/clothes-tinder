import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:clothes_tinder/features/home/data/product_data_access.dart';
import 'package:clothes_tinder/features/home/domain/prefetched_products.dart';
import 'package:clothes_tinder/features/home/domain/product_provider.dart';
import 'package:clothes_tinder/features/product_query/domain/product_query_holder.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  static const NUM_PRODUCTS_CACHED = 8;
  static const NUM_PRODUCTS_PER_FETCH = 5;

  ProductBloc() : super() {
    _nextProducts.addAll(getPrefetchedProducts());
    if (_nextProducts.isEmpty){
       hasNextProduct = ValueNotifier(false);
    } else {
      hasNextProduct = ValueNotifier(true);
    }
    locator<ProductQueryHolder>().addListener(_onQueryChange);
    runFetchIfNecessary();
  }

  Future<void> runFetchIfNecessary() async {
    print("ATTEMPTING FETCH");
    if (_nextProducts.length < NUM_PRODUCTS_CACHED && !noMoreToFetch.value) {
      if (isFetching.value) {
        return; //prevents starting many fetches due to calls from different places
      }
      isFetching.value = true;
      final List<Product> productList = await runProductFetch();
      isFetching.value = false;
      if (productList.length < NUM_PRODUCTS_PER_FETCH) {
        noMoreToFetch.value = true;
      }

      //doing it this way allows fetched products to be placed
      //on bottom of stack
      productList.addAll(_nextProducts);
      _nextProducts = productList;
      hasNextProduct.value = true;
      runFetchIfNecessary(); //will not run if there is no more to fetch
    }
  }

  @override
  ProductState get initialState => ProductInitial();

  ValueNotifier<bool> noMoreToFetch = ValueNotifier(false);
  ValueNotifier<bool> isFetching = ValueNotifier(false);
  ValueNotifier<bool> hasNextProduct;

  final List<Product> _seenProducts = [];
  List<Product> _nextProducts = [];

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    runFetchIfNecessary();
    if (event is UndoProductEvent) {
      yield tryPrevProduct();
    } else if (event is NextProductEvent) {
      yield tryNextProduct();
    }
  }

  ProductState tryNextProduct() {
    if (_nextProducts.isEmpty) {
      return NoProductState();
    } else {
      return DisplayProductState(
          product: _nextProducts.removeLast(),
          cardCameFrom: CardCameFrom.forward);
    }
  }

  ProductState tryPrevProduct() {
    if (_seenProducts.isEmpty) {
      return NoUndoState();
    } else {
      return DisplayProductState(
          product: _seenProducts.removeLast(), cardCameFrom: CardCameFrom.undo);
    }
  }

  ProductState peekPrevProduct() {
    if (_seenProducts.isEmpty) {
      return NoProductState();
    } else {
      return DisplayProductState(
          product: _seenProducts.elementAt(_seenProducts.length - 1));
    }
  }

  void _onQueryChange(){
    print("QUERY CHANGED");
    _nextProducts.clear();
    runFetchIfNecessary();
  }

  void addSeen(ProductState bumpForward) {
    if (bumpForward is DisplayProductState) {
      _seenProducts.add(bumpForward.product);
    }
  }

  void addNext(ProductState bumpForward) {
    if (bumpForward is DisplayProductState) {
      _nextProducts.add(bumpForward.product);
    }
  }
}
