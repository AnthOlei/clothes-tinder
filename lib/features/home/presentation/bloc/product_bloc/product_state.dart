part of 'product_bloc.dart';

enum CardCameFrom{
  forward,
  undo,
}

@immutable
abstract class ProductState {}

class ProductInitial extends ProductState {}

class DisplayProductState extends ProductState {
  final Product product;
  final CardCameFrom cardCameFrom;

  DisplayProductState({this.product, this.cardCameFrom});
}

class NoProductState extends ProductState {}

class NoUndoState extends ProductState {}

