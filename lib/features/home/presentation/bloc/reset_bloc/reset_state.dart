part of 'reset_bloc.dart';

@immutable
abstract class ResetState {}

class ResetInitial extends ResetState {}

class ResetCards extends ResetState {}