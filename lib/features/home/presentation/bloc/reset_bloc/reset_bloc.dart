import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'reset_event.dart';
part 'reset_state.dart';

class ResetBloc extends Bloc<ResetEvent, ResetState> {
  @override
  ResetState get initialState => ResetInitial();

  @override
  Stream<ResetState> mapEventToState(
    ResetEvent event,
  ) async* {
    yield ResetCards();
  }
}
