part of 'reset_bloc.dart';

@immutable
abstract class ResetEvent {}

class ResetChildrenEvent extends ResetEvent {}