import 'package:json_annotation/json_annotation.dart';

part 'product_data_access.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ProductQuery {
  //all price bounds are dollars. ignore cents, level of percision not needed.
  int priceUpperBound;
  int priceLowerBound;
  List<int> seen;

  //names of fk's
  String kind;
  String brand;
  String color;

  ProductQuery(
      {this.brand,
      this.priceLowerBound,
      this.priceUpperBound,
      this.seen,
      this.kind,
      this.color});

  ProductQuery.empty() {
    seen = [];
  }

  factory ProductQuery.fromJson(Map<String, dynamic> json) =>
      _$ProductQueryFromJson(json);

  Map<String, dynamic> toJson() => _$ProductQueryToJson(this);
}
