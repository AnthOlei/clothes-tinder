// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_data_access.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductQuery _$ProductQueryFromJson(Map<String, dynamic> json) {
  return ProductQuery(
    brand: json['brand'] as String,
    priceLowerBound: json['price_lower_bound'] as int,
    priceUpperBound: json['price_upper_bound'] as int,
    seen: (json['seen'] as List)?.map((e) => e as int)?.toList(),
    kind: json['kind'] as String,
    color: json['color'] as String,
  );
}

Map<String, dynamic> _$ProductQueryToJson(ProductQuery instance) =>
    <String, dynamic>{
      'price_upper_bound': instance.priceUpperBound,
      'price_lower_bound': instance.priceLowerBound,
      'seen': instance.seen,
      'kind': instance.kind,
      'brand': instance.brand,
      'color': instance.color,
    };
