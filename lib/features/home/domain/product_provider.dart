import 'package:clothes_tinder/features/home/data/product_data_access.dart';
import 'package:clothes_tinder/features/product_query/domain/product_query_holder.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/models/product_query.dart';

Future<List<Product>> runProductFetch() async {
  final queryholder = locator<ProductQueryHolder>();
  final response = (await queryProducts(queryholder.query))["products"];
  final List<Product> products = [];
  for (final element in response){
    Product product = Product.fromJson(element as Map<String, dynamic>);
    products.add(product);
  }
  return products;
}