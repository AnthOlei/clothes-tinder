import 'package:clothes_tinder/features/home/presentation/bloc/product_bloc/product_bloc.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/self_removing_lister.dart';

class TripleProductStack {
  ProductState next; //BOTTOM
  ProductState curr; //DISPLAYED

  ProductBloc _bloc;

  TripleProductStack(ProductBloc bloc) {
    _bloc = bloc;
    final locatorProduct = locator<Product>();
    if (locatorProduct.title != null) {
      curr = DisplayProductState(
          product: locatorProduct, cardCameFrom: CardCameFrom.forward);
    } else {
      curr = NoProductState();
    }
    if (bloc.isFetching.value) {
      createSelfRemovingListener(_replaceNextWithProduct, bloc.hasNextProduct);
    }
    next = NoProductState();
  }

  ProductState bumpForward(ProductState newState) {
    final pushedOff = curr;
    curr = next;
    next = newState;
    _onEveryShift();
    return pushedOff;
  }

  ProductState bumpBackward(ProductState undidProduct) {
    final pushedOff = next;
    next = curr;
    curr = undidProduct;
    _onEveryShift();
    return pushedOff;
  }

  void _replaceNextWithProduct() {
    if (next is NoProductState) {
      next = _bloc.tryNextProduct();
    }
  }

  void _onEveryShift() {
    final currState = curr;
    if (currState is DisplayProductState) {
      locator<Product>().override(currState.product);
    }
  }
}
