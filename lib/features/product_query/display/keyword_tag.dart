import 'package:clothes_tinder/features/product_query/domain/product_query_holder.dart';
import 'package:flutter/material.dart';

class KeywordTag extends StatelessWidget {
  final String name;
  final Color color;
  final QueryParam param;
  final void Function(QueryParam) removeTag;

  const KeywordTag({this.name, this.param, this.color, this.removeTag});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, right: 8.0),
      child: Container(
        decoration: BoxDecoration(
            color: color,
            borderRadius: const BorderRadius.all(Radius.circular(16))),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            children: <Widget>[
              Text(
                name,
                style: const TextStyle(color: Colors.white),
              ),
              Container(
                width: 10,
              ),
              GestureDetector(
                onTap: () {
                  removeTag(param);
                },
                child: Icon(
                  Icons.cancel,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
