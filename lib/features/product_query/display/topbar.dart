import 'dart:math';

import 'package:clothes_tinder/features/home/data/product_data_access.dart';
import 'package:clothes_tinder/features/product_query/domain/product_query_holder.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/search_terms.dart';
import 'package:clothes_tinder/stdlib/ui/animated_button.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:clothes_tinder/stdlib/ui/toaster.dart';
import 'package:flutter/material.dart';

import 'keyword_tag.dart';

class TopBar extends StatefulWidget {
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  TextEditingController inputKey = TextEditingController();
  int catagoryIndex = 0;
  int secondaryDropdownIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 4.0),
                child: DropdownButton<String>(
                  elevation: 2,
                  underline: Container(),
                  isDense: true,
                  itemHeight: 200,
                  items: searchCatagories.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  value: searchCatagories[catagoryIndex],
                  onChanged: (val) {
                    setState(() {
                      catagoryIndex = searchCatagories.indexOf(val);
                      secondaryDropdownIndex =
                          0; //always reset to avoid out of bounds!
                    });
                  },
                ),
              ),
              _determineFirstChild(),
              _determineSecondChild(),
              AnimatedButton(
                onClick: _addTag,
                size: AnimatedButtonSize.extrasmall,
                baseIconColor: Colors.white,
                backgroundColor: AppColors.primary,
                iconSizePercent: 1.3,
                icon: Icons.keyboard_return,
              )
            ],
          ),
          Container(
            height: 1,
            width: MediaQuery.of(context).size.width * 0.7,
            color: Colors.black26,
          ),
          Container(
            height: 50,
            child: !locator<ProductQueryHolder>().isEmpty()
                ? ListView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: _buildTagsBasedOnQuery(),
                  )
                : const Center(
                    child: Text(
                    "Added search terms will appear here...",
                    style: TextStyle(
                      color: Colors.black26,
                    ),
                  )),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildTagsBasedOnQuery() {
    final ProductQuery query = locator<ProductQueryHolder>().query;
    List<Widget> tags = [];
    if (query.brand != null) {
      tags.add(_buildTag("Brand: ${query.brand}", QueryParam.brand));
    }

    if (query.priceLowerBound != null) {
      tags.add(
          _buildTag(">\$${query.priceLowerBound}", QueryParam.priceLowerBound));
    }

    if (query.priceUpperBound != null) {
      tags.add(_buildTag(
          "< \$${query.priceUpperBound}", QueryParam.priceLowerBound));
    }

    if (query.kind != null) {
      tags.add(_buildTag("Catagory: ${query.kind}", QueryParam.kind));
    }

    if (query.color != null) {
      tags.add(_buildTag("Color: ${query.color}", QueryParam.color));
    }
    return tags;
  }

  //need to dertermine first and second child for all cases.
  //because we can't have a null child, must return empty container if
  //no need for second child.
  //switch cases are determied in search_terms file index variables.
  Widget _determineFirstChild() {
    Widget retVal = Container();
    switch (catagoryIndex) {
      case 0: //color
        retVal = _buildDropdown(colors, true);
        break;
      case 1: //brand
        retVal = _buildDropdown(brands, true);
        break;
      case 2: //price
        retVal = _buildDropdown(comparisons, false);
        break;
      case 3:
        retVal = _buildDropdown(catagories, true);
        break;
    }
    return retVal;
  }

  Widget _determineSecondChild() {
    Widget retVal = Container();
    switch (catagoryIndex) {
      case 2:
        retVal = Expanded(
          child: TextField(
            keyboardType: TextInputType.number,
            controller: inputKey,
            decoration: const InputDecoration(
              border: InputBorder.none,
              hintText: "Enter price",
            ),
            onSubmitted: (_) {
              _addTag();
            },
          ),
        );
        break;
    }
    return retVal;
  }

  Widget _buildDropdown(List<String> values, bool expanded) {
    final Widget dropDown = Padding(
      padding: const EdgeInsets.only(right: 4.0),
      child: DropdownButton<String>(
        isExpanded: expanded,
        elevation: 2,
        underline: Container(),
        isDense: true,
        itemHeight: 200,
        items: values.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        value: values[secondaryDropdownIndex],
        onChanged: (val) {
          setState(() {
            secondaryDropdownIndex = values.indexOf(val);
          });
        },
      ),
    );
    if (expanded) {
      return Expanded(child: dropDown);
    } else {
      return dropDown;
    }
  }

  void _addTag() {
    String tagValue = "${searchCatagories[catagoryIndex]} ";
    final String valueTrimmed = inputKey.value.text.trim();
    switch (catagoryIndex) {
      case 0:
        locator<ProductQueryHolder>().color = colors[secondaryDropdownIndex];
        break;
      case 1:
        locator<ProductQueryHolder>().brand = brands[secondaryDropdownIndex];
        break;
      case 2:
        if (valueTrimmed == "") return;
        int value;
        print(valueTrimmed);
        //cast to double first, so we can accomidate for
        //values like 12.12 which wouldnt cast to int
        try {
          value = int.parse(valueTrimmed.split(".")[0]);
        } on FormatException catch (e) {
          warningToast("Please only input numbers");
        }
        if (comparisons[secondaryDropdownIndex] == ">") {
          locator<ProductQueryHolder>().lowerBound = value;
        } else if (comparisons[secondaryDropdownIndex] == "<") {
          locator<ProductQueryHolder>().upperBound = value;
        }
        break;
      case 3:
        tagValue += catagories[secondaryDropdownIndex];
          locator<ProductQueryHolder>().kind = catagories[secondaryDropdownIndex];
        break;
    }
    setState(() {});
    inputKey.clear();
  }

  Widget _buildTag(String text, QueryParam param) {
    return KeywordTag(
      color: AppColors.primary,
      param: param,
      name: text,
      removeTag: _removeTag,
    );
  }

  void _removeTag(QueryParam param) {
    locator<ProductQueryHolder>().removeParam(param);
    setState(() {});
  }
}
