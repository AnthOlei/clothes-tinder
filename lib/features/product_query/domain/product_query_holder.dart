import 'package:clothes_tinder/features/home/data/product_data_access.dart';
import 'package:flutter/material.dart';

enum QueryParam {
  brand,
  priceLowerBound,
  priceUpperBound,
  kind,
  color,
}

//NOTE: seen is a HIDDEN property of the query, and is managed behind the scenes.
class ProductQueryHolder extends ChangeNotifier {
  ProductQuery query;

  ProductQueryHolder({this.query});

  set brand(String value) {
    query.brand = value;
    notifyListeners();
  }

  String get brand => query.brand;

  set lowerBound(int value) {
    query.priceLowerBound = value;
    notifyListeners();
  }

  int get lowerBound => query.priceLowerBound;

  set upperBound(int value) {
    query.priceUpperBound = value;
    notifyListeners();
  }

  int get upperBound => query.priceUpperBound;

  set kind(String value) {
    query.kind = value;
    notifyListeners();
  }

  String get kind => query.kind;

  set color(String value) {
    query.color = value;
    notifyListeners();
  }

  String get color => query.color;

  void addSeen(int id) {
    query.seen.add(id);
  }

  void removeSeen(int id) {
    query.seen.remove(id);
  }

  bool isEmpty() {
    return !(query.brand != null ||
        query.color != null ||
        query.kind != null ||
        query.priceLowerBound != null ||
        query.priceUpperBound != null);
  }

  void removeParam(QueryParam param) {
    switch (param) {
      case QueryParam.brand:
        brand = null;
        return;
      case QueryParam.priceLowerBound:
        lowerBound = null;
        return;
      case QueryParam.priceUpperBound:
        upperBound = null;
        return;
      case QueryParam.kind:
        kind = null;
        return;
      case QueryParam.color:
        color = null;
        return;
    }
    notifyListeners();
  }
}
