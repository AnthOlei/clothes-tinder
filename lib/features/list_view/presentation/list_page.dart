import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/models/product_list.dart';
import 'package:clothes_tinder/stdlib/models/product_lists.dart';
import 'package:clothes_tinder/stdlib/ui/action_helpers/dialog.dart';
import 'package:clothes_tinder/stdlib/ui/secondary_page.dart';
import 'package:flutter/material.dart';

import 'liked_product_display.dart';

class ListPage extends StatefulWidget {
  final ProductList list;

  const ListPage({@required this.list});

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  String searchTerm = "";

  @override
  Widget build(BuildContext context) {
    return SecondaryPage(
      title: widget.list.name,
      hasSearch: true,
      textfieldPlaceholder: "Search ${widget.list.products.length} Products...",
      onSearch: _onSearch,
      elements: _buildElementList(),
    );
  }

  List<Widget> _buildElementList() {
    final List<Product> products = widget.list.products;
    final List<Widget> likedProductDisplays = [];
    for (final product in products) {
      final String manufacturerNullAware = (product.manufacturer ?? "").toLowerCase();
      if (product.title.toLowerCase().contains(searchTerm) | manufacturerNullAware.contains(searchTerm)){
        likedProductDisplays.add(LikedProductDisplay(product: product, buyProduct: _buyProduct, removeProduct: _removeProduct,));
      }
    }
    return likedProductDisplays;
  }

  void _removeProduct(Product product){
    void _confirmRemove(Product product){
      //because it is a refrence, it deletes it from 
      //the actual list. :)
      widget.list.products.remove(product);
      setState(() {});
      Navigator.of(context).pop();
    }

    showAppDialog(
      body: Text("Are you sure you'd like to remove ${product.title} from ${widget.list.name}?"),
      buttonText: "Delete",
      title: "Remove Item",
      context: context,
      onPressed: (){_confirmRemove(product);}
    );
  }

  void _buyProduct(Product product){
        showAppDialog(
      body: Text("This product was found on LOCATION. Would you like to continue with the purchase?"),
      buttonText: "Continue",
      title: "Continue with purchase of ${product.title}?",
      context: context,
      onPressed: (){print("REDIRECT!");}
    );
  }

  void _onSearch(String searchString) {
    setState(() {
      searchTerm = searchString.toLowerCase();
    });
  }
}
