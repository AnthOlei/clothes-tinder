import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:clothes_tinder/stdlib/ui/animated_flat_button.dart';
import 'package:clothes_tinder/stdlib/ui/image_gallary_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class LikedProductDisplay extends StatefulWidget {
  final Product product;
  final void Function(Product) buyProduct;
  final void Function(Product) removeProduct;

  const LikedProductDisplay(
      {this.product, this.buyProduct, this.removeProduct});

  @override
  _LikedProductDisplayState createState() => _LikedProductDisplayState();
}

class _LikedProductDisplayState extends State<LikedProductDisplay> {
  int currImageIndex = 0;
  final NumberFormat _formatCurrency = NumberFormat.simpleCurrency();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 5.0),
            child: Stack(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(widget.product.images[currImageIndex].url)),
                ImageGallaryBar(
                  currentImage: currImageIndex,
                  totalPhotos: widget.product.images.length,
                ),
                Positioned(
                    top: 0,
                    left: 0,
                    right: MediaQuery.of(context).size.width / 2 - 1,
                    bottom: 0,
                    child: GestureDetector(
                      onTap: _prevImage,
                    )),
                Positioned(
                    top: 0,
                    right: 0,
                    left: MediaQuery.of(context).size.width / 2 - 1,
                    bottom: 0,
                    child: GestureDetector(
                      onTap: _nextImage,
                    )),
                Positioned(
                  right: 10,
                  bottom: 10,
                  child: Row(
                    children: <Widget>[
                      AnimatedFlatButton(
                        icon: Icons.delete,
                        baseBackgroundColor: Colors.red[200],
                        backgroundToColor: Colors.red[800],
                        iconSizePercent: 0.9,
                        size: AnimatedButtonSize.standard,
                        onClick: () {
                          widget.removeProduct(widget.product);
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: AnimatedFlatButton(
                          icon: Icons.shopping_cart,
                          baseBackgroundColor: Colors.blue[200],
                          backgroundToColor: Colors.blue[800],
                          iconSizePercent: 0.9,
                          size: AnimatedButtonSize.standard,
                          onClick: () {
                            widget.buyProduct(widget.product);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Text(
            _buildDesciption(),
            style: TextStyle(fontSize: 19),
          ),
        ],
      ),
    );
  }

  String _buildDesciption(){
    String desc = "${widget.product.title}, ${_formatCurrency.format(widget.product.price)}";
    desc += widget.product.manufacturer ?? "";
    desc += "\n";
    desc += widget.product.details ?? "";
    return desc;
  }

  void _nextImage() {
    if (currImageIndex >= widget.product.images.length) {
      setState(() {
        currImageIndex++;
      });
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.lightImpact',
      );
    } else {
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.mediumImpact',
      );
    }
  }

  _prevImage() {
    if (currImageIndex != 0) {
      setState(() {
        currImageIndex++;
      });
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.lightImpact',
      );
    } else {
      SystemChannels.platform.invokeMethod<void>(
        'HapticFeedback.vibrate',
        'HapticFeedbackType.mediumImpact',
      );
    }
  }
}
