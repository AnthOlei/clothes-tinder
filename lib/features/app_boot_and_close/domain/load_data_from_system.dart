import 'dart:async';

import 'package:clothes_tinder/stdlib/filesystem/simple_file_system.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';
import 'package:flutter/cupertino.dart';

Future<void> loadDataFromSystem(BuildContext context) async {
  List<Future> tasks = [];
  tasks.add(_loadTopProduct(context));
  //tasks.add(_loadUserData(context));
  await Future.wait(tasks);
}


//SHOULD WE DO THIS????
//goal: preloads the first product's images and doesn't load the user
// in until the images are loaded. be
Future<void> _loadTopProduct(BuildContext context) async {
  try {
  final Product topProduct = Product.fromJson(await readFromFileSystem("TopProductCard") as Map<String, dynamic>);
  locator<Product>().override(topProduct);
  } on NoFileException catch (e) {
    print("couldn't find top file");
  }
}
