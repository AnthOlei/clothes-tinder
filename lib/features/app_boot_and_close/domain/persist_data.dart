import 'package:clothes_tinder/stdlib/filesystem/simple_file_system.dart';
import 'package:clothes_tinder/stdlib/locator.dart';
import 'package:clothes_tinder/stdlib/models/product.dart';

Future<void> persistAllNecessary() async {
  writeToFileSystem("TopProductCard",locator<Product>().toJson());
  print("persisted TopProductCard");
  //persist user data
}