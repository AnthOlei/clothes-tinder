import 'package:clothes_tinder/features/app_boot_and_close/domain/persist_data.dart';
import 'package:flutter/cupertino.dart';

class LifecycleEventHandler extends WidgetsBindingObserver {
  LifecycleEventHandler();

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
        return;
      case AppLifecycleState.paused:
        persistAllNecessary();
        return;
      case AppLifecycleState.detached:
        persistAllNecessary();
        return;
      case AppLifecycleState.resumed:
        return;
    }
  }
}
