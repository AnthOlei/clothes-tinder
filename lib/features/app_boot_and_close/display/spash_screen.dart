import 'package:auto_route/auto_route.dart';
import 'package:clothes_tinder/features/app_boot_and_close/domain/load_data_from_system.dart';
import 'package:clothes_tinder/stdlib/ui/app_colors.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _beginLoadingData(context);
    return Container(
      decoration: BoxDecoration(gradient: AppColors.gradient),
      child: const Center(
          child: Text(
        "Swipe\nShop",
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white,
        fontFamily: "DancingScript",
        fontSize: 60,
        decoration: TextDecoration.none,
        ),
      )),
    );
  }

  Future<void> _beginLoadingData(BuildContext context) async {
    await loadDataFromSystem(context);
    ExtendedNavigator.of(context).pushReplacementNamed("/app");
  }
}
